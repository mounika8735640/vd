const express = require('express');
const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;

const fetch = express.Router().get("/", (req, res) => {
    MongoClient.connect("mongodb://127.0.0.1:27017/Blackcoffer", (err, client) => {
        if (err) {
            throw err;
        } else {
            console.log("Connection Established...");

            const db = client.db('Blackcoffer'); // Get the database reference

            db.collection("VD").find({}).toArray((err, Users) => {
                if (err) {
                    throw err;
                } else {
                    res.send(Users);
                }
            });
        }
        client.close();
    });
});

module.exports = fetch;
